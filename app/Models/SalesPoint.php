<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesPoint extends Model
{
    use HasFactory;
    public function somme()
    {
        return SalesPointStock::where('sales_point_id',$this->id)->sum('quantity');
    }

}
