<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nodemcu extends Model
{
    use HasFactory;
    protected $table='nodemcu_data'; 
    protected $fillable = [
        'salesPoint_id',
        'product_id',
        'productName',
    ];
}
