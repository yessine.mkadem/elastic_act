<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory;

    public function stock()
    {
        return $this->hasMany(Stock::class ,'warehouse_id','id');
    }
}
