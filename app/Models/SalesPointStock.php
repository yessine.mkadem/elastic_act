<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesPointStock extends Model
{
    use HasFactory;

    public function produits()
    {
        return $this->belongsTo(Product::class,'product_id','name');
    }
}
