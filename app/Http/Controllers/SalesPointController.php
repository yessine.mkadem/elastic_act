<?php

namespace App\Http\Controllers;

use App\Models\Lot;
use App\Models\Stock;
use App\Models\SalesPointStock;
use App\Models\Product;
use App\Models\Warehouse;
use App\Models\SalesPoint;
use Illuminate\Http\Request;
use Psy\Readline\Hoa\Console;

class SalesPointController extends Controller
{
    //
    public function index()
    {

        $stock = SalesPoint::where('stat',1)->get();
        $stocks = Stock::all();
        $products =Product::where('stat',1)->get();
        $warehouses =Warehouse::where('stat',1)->get();
        $salesStock = SalesPointStock::get();
        //dd($stock);
        $sum = SalesPointStock::sum('quantity');

       // dd($sum);
        //->sum('quantity')

       return view('salespoint.index', ['sales_points' => $stock,'warehouses'=> $warehouses ,
       'products'=>$products, 'stocks' => $stocks,'sum'=>$sum ]);
    }
    public function create()
    {
        return view('salespoint.create');
    }
    public function store(Request $req )
    {
        //dd($req);
        $req->validate([
            'name' => 'required',
            'emplacement' => 'required',
          ]);
         //$modif=true;
         $coffee = SalesPoint::find($req->id);
        

         if(!$coffee){
           $coffee = new SalesPoint;
         }  
        $coffee->name=$req->name;
        $coffee->emplacement=$req->emplacement;
        $coffee->save();

        
        // get the id of the salespoint saved
        $newSalespointId = $coffee->id;

        // search in salespointstock 
       $foundSalespointStock= SalesPointStock::find($newSalespointId);
        // if found do nothing
        // if not found
        // create new salespointstock with the id of salespoint saved 
        if(!$foundSalespointStock){
         $s =  new SalesPointStock;
            $s->sales_point_id = $newSalespointId;
            $s->product_id  = "";
            $s->quantity = 0;
           
            $s->save();
        }

        return redirect()->route('salespoint.index');
    }
    
    // the edit function of warehouse
    public function edit($id)
    {
        $model = SalesPoint::find($id);
        return view('salespoint.create',['model'=> $model]);
    }
    // the archive function of warehouse
    public function archiver($id)
    {
        $model = SalesPoint::find($id);
        $model->stat='0';
        $model->save();
        return redirect()->route('salespoint.index');
    }
    public function show($id){

             // $id == sales_point_id 
       //$products_in_salepoint = SalesPointStock::where('sales_point_id',$id)->get();

       $salespoint_name = SalesPoint::find($id);
       $lots = Lot::where('salespoint',(string)$id)->get();
       //dd($lots);
       //dd($salespoint_name->name);
       /* foreach($products_in_salepoint as $pr)
       {
        if ($pr->quantity <20)
       {
        session()->flash('message', 'the stock is less than the minimum quantity');
       }
       } */
        return view('salespoint.detail', ['lots' => $lots,
        'salespoint_name' => $salespoint_name ] );
    }

    public function newslot()
    {
        //dd($req);
        $salespoint_name = SalesPoint::get();
       // dd($salespoint_name);
          return view('salespoint.newslot', ['salespoint_name' =>$salespoint_name]);
    }

    public function newslotsave(Request $req)
    {
        
        $lots = new Lot;
          
        $lots->name=$req->name;
        $lots->salespoint=$req->salespoint;
        //dd($lots);
        $lots->save();
        return redirect()->route('salespoint.index');
    }
}
