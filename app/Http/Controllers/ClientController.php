<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class ClientController extends Controller
{
    //
    public function index()
    {

        $stock = User::where('stat',1)->get();

       return view('client.index', ['users' => $stock]);
    }
    public function create()
    {
        return view('client.create');
    }
    public function store(Request $req )
    {
        //dd($req);
        $req->validate([
            'name' => 'required',
            'rfid' => 'required',
          ]);
         //$modif=true;
         $coffee = User::find($req->id);

         if(!$coffee){
           $coffee = new User;
         } 
        $coffee->name=$req->name;
        $coffee->email=$req->email;
        $coffee->password=$req->password;
        $coffee->rfid=$req->rfid;
        $coffee->save();

        return redirect()->route('client.index');
    }
    // the edit function of product
    public function edit($id)
    {
        $model = User::find($id);
        return view('client.create',['model'=> $model]);
    }
    // the archive function of warehouse
    public function archiver($id)
    {
        $model = User::find($id);
        $model->stat='0';
        $model->save();
        return redirect()->route('client.index');
    }
}
