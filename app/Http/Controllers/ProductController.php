<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    //
    //
    public function index()
    {

        $stock = Product::where('stat',1)->get();

       return view('product.index', ['products' => $stock]);
    }
    public function create()
    {
        return view('product.create');
    }
    public function store(Request $req )
    {
        //dd($req);
        $req->validate([
            'name' => 'required',
            'description' => 'required',
          ]);
         //$modif=true;
         $coffee = Product::find($req->id);

         if(!$coffee){
           $coffee = new Product;
         } 
        $coffee->name=$req->name;
        $coffee->description=$req->description;
        $coffee->save();

        return redirect()->route('product.index');
    }
     
    // the edit function of product
    public function edit($id)
    {
        $model = Product::find($id);
        return view('product.create',['model'=> $model]);
    }
    // the archive function of warehouse
    public function archiver($id)
    {
        $model = Product::find($id);
        $model->stat='0';
        $model->save();
        return redirect()->route('product.index');
    }
}
