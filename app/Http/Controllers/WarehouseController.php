<?php

namespace App\Http\Controllers;


use App\Models\Warehouse;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
   public function index()
    {

        $stock = Warehouse::where('stat',1)->get();

       return view('warehouse.index', ['stock' => $stock]);
    }
    public function create()
    {
        return view('warehouse.create');
    }
    public function store(Request $req )
    {
        $req->validate([
            'name' => 'required',
            'description' => 'required',
          ]);
         //$modif=true;
         $coffee = Warehouse::find($req->id);

         if(!$coffee){
           $coffee = new Warehouse;
         }  

        //dd($req);
        $coffee->name=$req->name;
        $coffee->description=$req->description;
        $coffee->save();

        return redirect()->route('warehouses.index');
    }
    // the edit function of warehouse
    public function edit($id)
    {
        $model = Warehouse::find($id);
        return view('warehouse.create',['model'=> $model]);
    }
    // the archive function of warehouse
    public function archiver($id)
    {
        $model = Warehouse::find($id);
        $model->stat='0';
        $model->save();
        return redirect()->route('warehouses.index');
    }

    
    
    
}
