<?php

namespace App\Http\Controllers;

use App\Models\Lot;
use App\Models\Stock;
use App\Models\Warehouse;
use App\Models\SalesPoint;
use Illuminate\Http\Request;
use App\Models\SalesPointStock;

class SalesPointStockController extends Controller
{
    //
    public function transferSales($id)
    { //dd($id);

        $model = Stock::find($id);
        $warehouses = Warehouse::where('stat', 1)->where('id', '!=', $model->warehouse_id)->get();
        $salespoint = SalesPoint::where('stat', 1)->get();
        $lots = Lot::get();
       
        return view('stock.transferSalespoint', ['warehouses' => $warehouses,
         'salespoint' => $salespoint, 'model' => $model,'lots'=>$lots]);
    }

    public function saveTransferSales(Request $req)
    {
        //dd($req->all());

        $model = Stock::find($req->id);
        $model->quantity = $model->quantity - $req->quantity;
        $model->save();
        
        //$lname = Lot::find($req->slot_dist_id);
      //dd($lname->name);
       //$lots = Lot::find($req->slot_dist_id);
        // call the lot line corresponding to the selected slot

       $lots = Lot::where('id',$req->slot_dist_id)->where('salespoint',$req->salespoint_dist_id)->first();
        //dd($lots);
    //     $lots = Lot::firstOrCreate(
    //       ['name' => $req->slot_dist_id],
    //       ['quantity' => $req->quantity],
    //       ['product' => $req->product_id, 'salespoint' => $req->salespoint_dist_id]
    //   );
   //dd($lots);
        if (!$lots)
        {
             $namelot = Lot::find($req->slot_dist_id);
             $lots = Lot::create(['name' => $namelot->name, 
             'quantity' => $req->quantity,
             'salespoint' => $req->salespoint_dist_id,
             'product' => $req->product_id
        ]);
         }
        //dd($lots);
       

        $add_to_stock = true;
        $salespointStock = SalesPointStock::where('sales_point_id', $req->salespoint_dist_id)
            ->where('product_id', $req->product_id)->first();
        //dd($salespointStock);
        if (!$salespointStock) {
            $salespointStock = new SalesPointStock();
            $add_to_stock = false;
        }
        $salespointStock->sales_point_id = $req->salespoint_dist_id;
        $salespointStock->product_id = $req->product_id;

        $lots->product = $req->product_id;
        $lots->salespoint = $req->slot_dist_id;
        //dd($lots);
        if($add_to_stock){
            $salespointStock->quantity = $salespointStock->quantity + $req->quantity;
            
            $lots->quantity = $lots->quantity + $req->quantity;
            $lots->product = $req->product_id;
            $lots->salespoint = $req->salespoint_dist_id;
            //dd($lots);
            
        }else{
            $salespointStock->quantity = $req->quantity;
            $lots->quantity = $req->quantity;
            
        } 
        $salespointStock->save();
        $lots->save();

        return redirect()->route('stocks.index');
    }
}
