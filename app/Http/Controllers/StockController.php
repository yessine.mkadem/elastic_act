<?php

namespace App\Http\Controllers;

use App\Models\Lot;
use Illuminate\Http\Request;
use App\Models\Warehouse;
use App\Models\Product;
use App\Models\SalesPoint;
use App\Models\Stock;

class StockController extends Controller
{
    public function index()
    {

        $stocks = Stock::all();

       return view('stock.index', ['stocks' => $stocks]);
    }
    public function create()
    {
        $products =Product::where('stat',1)->get();
        $warehouses =Warehouse::where('stat',1)->get();
        return view('stock.create',['warehouses'=> $warehouses , 'products'=>$products]);
    }
    public function store(Request $req )
    {
        //dd($req->all());

        $req->validate([
            'warehouse_id' => 'required',
            'product_id' => 'required',
            'quantity' => 'required',
          ]);
          $add_to_stock = false;
          $coffee = Stock::find($req->id);
          
          if(!$coffee){
            $coffee = Stock::where('warehouse_id',$req->warehouse_id)->where('product_id',$req->product_id)
            ->first();
            $add_to_stock = true;
            if(!$coffee){
                $coffee = new Stock;
              }
          }

        $coffee->warehouse_id=$req->warehouse_id;
        $coffee->product_id=$req->product_id;
        if(!$add_to_stock)
            $coffee->quantity=$req->quantity;
        else
            $coffee->quantity=$coffee->quantity+$req->quantity;

        $coffee->save();

        return redirect()->route('stocks.index');
    }

    public function edit($id)
    {
        $model = Stock::find($id);
        $products =Product::where('stat',1)->get();
        $warehouses =Warehouse::where('stat',1)->get();
        return view('stock.create',['warehouses'=> $warehouses ,
         'products'=>$products , 'model'=>$model]);
    }
    public function transfer($id)
    {//dd($id);

        $model = Stock::find($id);
        $warehouses = Warehouse::where('stat',1)->where('id','!=',$model->warehouse_id)->get();
        $salespoint = SalesPoint::where('stat',1)->where('id','!=',$model->warehouse_id)->get();

        return view('stock.transfer',['warehouses'=> $warehouses ,'salespoint'=>$salespoint, 'model'=>$model]);
    }

    public function saveTransfer(Request $req)
    {//dd($req->all());

        $model = Stock::find($req->id);
        $model->quantity = $model->quantity - $req->quantity;
        $model->save();

        $stock = Stock::where('product_id',$model->product_id)
        ->where('warehouse_id',$req->warehouse_dist_id)->first();
        if($stock){
            $stock->quantity = $stock->quantity + $req->quantity;
        }else{
            $stock = new Stock;
            $stock->quantity = $req->quantity;
            $stock->product_id = $model->product_id;
            $stock->warehouse_id = $req->warehouse_dist_id;
        }
        $stock->save();

        return redirect()->route('stocks.index');
    }
    //////
    public function transferSales($id)
    { //dd($id);

        $model = Stock::find($id);
        $warehouses = Warehouse::where('stat',1)->where('id','!=',$model->warehouse_id)->get();
        $salespoint = SalesPoint::where('stat',1)->where('id','!=',$model->warehouse_id)->get();
        
        return view('stock.transferSalespoint',['warehouses'=> $warehouses ,'salespoint'=>$salespoint,
         'model'=>$model]);
    }

    public function saveTransferSales(Request $req)
    {//dd($req->all());

        $model = Stock::find($req->id);
        $model->quantity = $model->quantity - $req->quantity;
        $model->save();

        $stock = Stock::where('product_id',$model->product_id)
        ->where('salespoint_id',$req->salespoint_dist_id)->first();
        if($stock){
            $stock->quantity = $stock->quantity + $req->quantity;
        }else{
            $stock = new Stock;
            $stock->quantity = $req->quantity;
            $stock->product_id = $model->product_id;
            $stock->salespoint_id = $req->salespoint_dist_id;
        }
        $stock->save();

        return redirect()->route('stocks.index');
    }
    public function getSlots(Request $req)
    {
        $slots = Lot::where('salespoint', $req->salespoint_dist)->get();
        $html = '';
        foreach($slots as $slot){
          $html .=  "<option value='".$slot->id."'>".$slot->name."</option>";
        }

        return response()->json(['html'=>$html]);
    }
    
}
