<?php

namespace App\Http\Controllers\Api;

use App\Models\Lot;
use App\Models\User;
use App\Models\Stock;
use App\Models\Nodemcu;
use App\Models\Product;
use App\Models\SalesPoint;
use App\Models\Consommation;
use Illuminate\Http\Request;
use App\Models\SalesPointStock;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SendDataController extends Controller
{
    // the function to send details of the salespoint 
    public function dataTransfer(Request $req)
    {   
        $model = Stock::find($req->id);
        //
        
        $stock = Stock::where('product_id',$model->product_id)
        ->where('warehouse_id',$model->warehouse_id)
        ->first();
       // $product = Product::find($stock->product_id);
        $salespoint = SalesPointStock::where('sales_point_id',$req->id)->get();
               
           
        if($stock){
            
            foreach($salespoint as $sal)
            {
               
                $product = Product::where('name',$sal->product_id)->get()->first();
                //dd($product->description);
                $data[] =
                [
                   "id" => $sal->id,
                   "sales_point_id" => intval($req->id), 
                   "name" => $sal->product_id,
                   "product_id" => $product->id,
                   "description" => $product->description,
                   "quantity" => $sal->quantity                   
                    
                ];
                
            }
        } 
        //dd($data);
        return response()->json($data);
    }     

    // the function to send salespoint total quantity
    public function sendSalespoint()
    {
       $salespoint = SalesPoint::all();
       // the data to be send to the api
       
       foreach($salespoint as $sales)
       {
       
        $data[] = [
            "id" => $sales->id,
            "name" => $sales->name,
            "emplacement" => $sales->emplacement,
            "quantity" => intval($sales->somme())           
        ];
        
       }       
       //$salesStock = SalesPointStock::find($point);
       
       return response()->json( $data );
    }


    public function selectProduct($id,$product_id)
    // there is another parameter that i should add which is user id 
    {
        
        $productname = Product::find($product_id);
        
        $nodeData = new Nodemcu;
        // identify salespoint
        $product_in_salespoint = SalesPointStock::where('sales_point_id',$id)
        ->where('product_id',$productname->name)->get()->first();

        // substract the quantity from the stock
        $product_in_salespoint->quantity = $product_in_salespoint->quantity - 1;

        // first check for the product if exist in the slot
        $slot = Lot::where('product',$productname->name)->get();
        $i = (int) 0;
        $quantities = [];
        $quant = (int) 0;
        // if product exist in one slot then check the quantity and give the user capsule
        if (count($slot) == 1)
        {
            $slot->quantity = $slot->quantity - 1;
            //save the changes in the lot data table
            $slot->save();
        }
        // else if product exist in more than one slot
        else if(count($slot) >= 2 )
        {  // this foreach is to define the slot that have the highest quantity
            foreach ($slot as $lo)
            {
                $quantities[$i] = $lo->quantity;
                
                if ($quantities[$i] > $quant)
                {
                    $quant = $quantities[$i];
                }
                $i = $i + 1;
            }
            // using this join method it will give the result 
           // $test = DB::table('lots')->join('products','lots.product','=','products.name')
            //->select('lots.quantity','products.name')
            //->get();
            //dd($test);

            $slotquantity = Lot::where('product',$productname->name)->where('quantity',$quant)
            ->get()->first();
            $slotquantity->quantity = $slotquantity->quantity -1;
        }

       $slotquantity->save();

        //dd($quantities);
        // save the operation of substraction in the database
        $product_in_salespoint->save();

        // fill the nodemcudata database
        $nodeData->salesPoint_id = $product_in_salespoint->sales_point_id;
        $nodeData->product_id = $productname->id;
        $nodeData->productName = $product_in_salespoint->product_id;

       // save the data added to nodemcudata
        $nodeData->save();

         $salesName = SalesPoint::find($product_in_salespoint->sales_point_id);

         $consommation = new Consommation;
         
        // added the data to the consommation table 

         $consommation->product = $productname->name;
         $consommation->salespoint = $salesName->name;
         // the usr will be replaced by the id that i will get from the request
         $consommation->user = "admin";

         //dd($consommation);
         $consommation->save();

       // dd($nodeData);
        return $product_in_salespoint;
    
    }


    //// function that send data to nodemcu
    public function nodemcuData()
    {
        $nodeData = Nodemcu::get()->first();
      
        if($nodeData)
        {
            // if exist 
            // transfer the data to the variable datatosend
            $DatatoSend = $nodeData->productName;
            //dd($DatatoSend);
            // delete all rows in the Nodemcu database
            Nodemcu::truncate();
            
            return $DatatoSend;

        }else{
            // null 
            return "no product";

        }
    }
}
