<style>
    .taille{
        min-width: 120px;
        margin-top:20px;
        font-weight: bold;
    }
    
</style>
<!--
    <div class ="card col-1 taille"style="background-color: #e3f2fd;" >
        <nav class ="navbar">
        <ul class ="nav navbar-nav">
        <li class ="nav-item">
        <a class ="nav-link" href="{{route('warehouses.index')}}"> Wharehouse </a>
        </li>
        <li class ="nav-item">
        <a class ="nav-link" href="{{route('salespoint.index')}}"> SalesPoint </a>
        </li>
        <li class ="nav-item">
        <a class ="nav-link" href="{{route('product.index')}}"> Product </a>
        </li>
        <li class ="nav-item">
        <a class ="nav-link" href="{{route('client.index')}}"> Clients </a>
        </li>
        <li class ="nav-item">
            <a class ="nav-link" href="{{route('stocks.index')}}"> Stock </a>
        </li>
        </ul>
        </nav>
        </div>-->
<style>
    .nav a:hover{
    color: white;
    }
    .nav a:active{
      color:white;
    }
    .icon-button {
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  color: #333333;
  background: #dddddd;
  border: none;
  outline: none;
  border-radius: 50%;
}

.icon-button:hover {
  cursor: pointer;
}

.icon-button:active {
  background: #cccccc;
}

.icon-button__badge {
  position: absolute;
  top: -6px;
  right: -6px;
  width: 20px;
  height: 20px;
  background: red;
  color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
}

</style>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('warehouses.index')}}">Warehouse</a>
                    </li>    
                  <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="{{route('salespoint.index')}}">SalesPoint</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('product.index')}}">Product</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('client.index')}}">Clients</a>
                  </li>
                  
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('stocks.index')}}">Stock</a>
                  </li>
                  <ul class="ml-auto">
                    <li class="nav-item">
                      <button type="button" class="icon-button" style="position: fixed; right: 17%;"
                      data-bs-toggle="modal" data-bs-target="#exampleModal">
                        <span class="material-symbols-outlined">notifications</span>
                        @if (session()->has('message'))
                            <span class="icon-button__badge" >1</span>
                        @else
                        <span class="icon-button__badge" >0</span>
                        @endif 
                        
                      </button>
                    </li>
                  </ul> 
                 
                </ul>
                
              </div>
            </div>
          </nav>  
         <!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Launch demo modal
</button> --}}

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="align-content: center">Alert Stock</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        The stock is less than the minimum quantity
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>