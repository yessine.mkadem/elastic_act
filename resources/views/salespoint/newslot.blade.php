@extends('layouts.app')

@section('content')
<style>
   .view{
      width: 400px;
      
   }
</style>
<div class="view mt-4">
    <h1 class="mb-4">New Slot</h1>
    <form action="{{route('salespoint.newslotsave')}}" method="post">
        @csrf
    <div class="form-group mb-2">
        <label for="salespoint" >Salespoint</label>
        <select name="salespoint" id="salespoint" class="form-control" required>
           <option  readonly>Selectionnez une machine</option>
          @foreach($salespoint_name as $sa)
          <option value="{{$sa->id}}">{{$sa->name}}</option>
          @endforeach
        </select>
       
    </div>
    <div class="form-group mb-2">
       <label for="name" >Slot Name</label>
       <input type="text" name="name" value="" class="form-control" required>
    </div>
    
    <a href="{{route('salespoint.index')}}" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary" style="background: blue">Save</button>
    </form>
   </div>
@endsection