@extends('layouts.app')

@section('content')

<div class="mt-4">
  
    <h3><span style="font-weight: bold;">
         {{ucfirst($salespoint_name->name)}}</span> Details :  </h3>
    <a href="{{URL::previous()}}" class="btn btn-secondary my-3" >back</a>
    <a href="{{route('salespoint.newslot')}}"  class="btn btn-success">New Slot</a>
   
    <table class="table  table-striped table-bordered taille" >
        <thead>
            <tr>
                <th>Slots List</th>
                <th>Products</th>
                <th>Quantity</th>
            </tr>
        </thead>
        <tbody>
         
         @foreach ($lots as $pr)
            <tr>
                
                <td>{{ $pr->name}}</td>
                <td>{{ $pr->product}}</td>
                <td>
                    {{$pr->quantity}}
                </td> 
                 
            </tr>
         @endforeach
              </tbody>
    </table>

@endsection