@extends('layouts.app')

@section('content')
<style>
   
    .taille{
        max-width: auto;
    }

    .icon-button {
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  color: #333333;
  background: #dddddd;
  border: none;
  outline: none;
  border-radius: 50%;
}

.icon-button:hover {
  cursor: pointer;
}

.icon-button:active {
  background: #cccccc;
}

.icon-button__badge {
  position: absolute;
  top: -6px;
  right: -6px;
  width: 20px;
  height: 20px;
  background: red;
  color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
}
</style>

    <div class="mt-4">
        
    <a href="{{route('salespoint.create')}}" class="btn btn-success">New Salespoint</a>
    </div>
    <div class="mt-4">
        <table class="table  table-striped table-bordered taille" >
            <thead>
                <tr>
                    <th>Salespoint Name</th>
                    <th>Emplacement</th>
                    <th>Quantity</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sales_points as $stock)
                <tr>
                    <td>{{ $stock->name }}</td>
                    <td>{{$stock->emplacement}}</td>
                      <td>
                        <span>{{$stock->somme()}}</span> 
                      </td>
                        <td>
                        <a href="{{route('salespoint.archiver',$stock->id)}}" class="btn btn-secondary">Achive</a>
                        <a href="{{route('salespoint.edit',$stock->id)}}" class="btn btn-info">Edit</a>
                        <a href="{{route('salespoint.show',$stock->id)}}" class="btn btn-success">Detail</a>
                       <!-- <a href=""  class="btn btn-danger">Delete</a>-->
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

 
@endsection