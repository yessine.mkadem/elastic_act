@extends('layouts.app')

@section('content')
<style>
   .view{
      width: 400px;
      
   }
</style>
<div class="view mt-4">
    <h1 class="mb-4">New SalesPoint</h1>
    <form action="{{route('salespoint.store')}}" method="post">
        @csrf
    <div class="form-group">
       <label for="salespoint" >SalesPoint Name</label>
       <input type="text" name="name" value="{{@$model->name}}" class="form-control" required>
    </div>
    <div class="form-group mb-2">
       <label for="emplacement">Emplacement</label>
       <textarea required name="emplacement"  class="form-control" >{{@$model->emplacement}} </textarea>
    </div>
    <input name="id" value="{{@$model->id}}"  type="hidden">
    <a href="{{route('salespoint.index')}}" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary" style="background: blue">Save</button>
    </form>
   </div>
@endsection