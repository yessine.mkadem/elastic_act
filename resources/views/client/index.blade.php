@extends('layouts.app')

@section('content')

    <div class="mt-4">
    <a href="{{route('client.create')}}" class="btn btn-success">New Client</a>
    </div>
    <div class="mt-4">
        <table class="table  table-striped table-bordered">
            <thead>
                <tr>
                    <th>Client Name</th>
                    <th>Email</th>
                    <th>RFID Code</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($users as $stock)
                <tr>
                    <td>{{ $stock->name }}</td>
                    <td>{{ $stock->email }}</td>
                    <td>{{$stock->rfid}}</td>
                    <td>
                        
                        <a href="{{route('client.archiver',$stock->id)}}" class="btn btn-secondary">Achive</a>
                        <a href="{{route('client.edit',$stock->id)}}" class="btn btn-info">Edit</a>
                       <!-- <a href=""  class="btn btn-danger">Delete</a>-->
                    </td>
                </tr>
                
            @endforeach
            </tbody>
        </table>
    </div>

 
@endsection