@extends('layouts.app')

@section('content')
<style>
   .view{
      width: 400px;
      
   }
</style>
<div class="view mt-4">
    <h1 class="mb-4">New Client</h1>
    <form action="{{route('client.store')}}" method="post">
        @csrf
    <div class="form-group">
         <label for="email" >Email</label>
         <input type="email" name="email" value="{{@$model->email}}" class="form-control" required>
    </div>    
    <div class="form-group">
      <label for="password" >Password</label>
      <input type="password" name="password" value="{{@$model->password}}" class="form-control" required>
 </div>
    <div class="form-group">
       <label for="name" >User Name</label>
       <input type="text" name="name" value="{{@$model->name}}" class="form-control" required>
    </div>
    <div class="form-group mb-2">
       <label for="rfid">RFID Code</label>
       <input required name="rfid" value="{{@$model->rfid}}" class="form-control" > 
    </div>
    <input name="id" value="{{@$model->id}}"  type="hidden">
    <a href="{{route('client.index')}}" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary">Save</button>
    </form>
   </div>
@endsection