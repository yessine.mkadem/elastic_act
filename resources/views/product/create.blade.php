@extends('layouts.app')

@section('content')
<style>
   .view{
      width: 400px;
      
   }
</style>
<div class="view mt-4">
    <h1 class="mb-4">New Product</h1>
    <form action="{{route('product.store')}}" method="post">
        @csrf
    <div class="form-group">
       <label for="product" >Product Name</label>
       <input type="text" name="name" value="{{@$model->name}}" class="form-control" required>
    </div>
    <div class="form-group mb-2">
       <label for="description">Description</label>
       <textarea required name="description"  class="form-control" > {{@$model->description}}</textarea>
    </div>
    <input name="id" value="{{@$model->id}}"  type="hidden">
    <a href="{{route('product.index')}}" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary" style="background: blue">Save</button>
    </form>
   </div>
@endsection