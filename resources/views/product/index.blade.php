@extends('layouts.app')

@section('content')

    <div class="mt-4">
    <a href="{{route('product.create')}}" class="btn btn-success" >New Product</a>
    </div>
    <div class="mt-4">
        <table class="table  table-striped table-bordered" >
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
           
            <tbody>
                @foreach ($products as $stock)
                <tr>
                    <td>{{ $stock->name }}</td>
                    <td>{{$stock->description}}</td>
                    <td>
                        
                        <a href="{{route('product.archiver',$stock->id)}}" class="btn btn-secondary">Achive</a>
                        <a href="{{route('product.edit',$stock->id)}}" class="btn btn-info">Edit</a>
                        <!--<a href=""  class="btn btn-danger">Delete</a>-->
                    </td>
                </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>

 
@endsection