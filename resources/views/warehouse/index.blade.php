@extends('layouts.app')

@section('content')

    <div class="mt-4">
    <a href="{{route('warehouses.create')}}" class="btn btn-success">New Wharehouse</a>
    </div>
    <div class="mt-4">
        <table class="table  table-striped table-bordered">
            <thead>
                <tr>
                    <th>Wharehouse Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($stock as $coffee)
                <tr>
                    <td >{{ $coffee->name }}</td>
                    <td>{{$coffee->description}}</td>
                    <td>
                        <a href="{{route('warehouses.archiver',$coffee->id)}}" class="btn btn-secondary" >Achive</a>
                        <a href="{{route('warehouses.edit',$coffee->id)}}" class="btn btn-info">Edit</a>
                        <!--<a href=""  class="btn btn-danger">Delete</a>-->
                    </td>
                </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>

 
@endsection