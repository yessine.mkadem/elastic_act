@extends('layouts.app')

@section('content')
<style>
   .view{
      width: 400px;
      
   }
</style>
<div class="view mt-4">
    <h1 class="mb-4">Transfer Stock</h1>
    <form action="{{route('stocks.saveTransfer')}}" method="post">
        @csrf
    <div class="form-group">
       <label for="warehouse_id" >From Warehouse</label>
       <input name="warehouse_id" class="form-control" required type="text" readonly 
       value="{{$model->warehouse->name}}">   
    </div>
    <div class="form-group">
      <label for="product_id" >Transfer the Product </label>
      <input name="product_id" class="form-control" required type="text" readonly 
       value="{{$model->product->name}}">
    </div>
      <div class="form-group">
         <label for="warehouse_dist_id" >To the Warehouse</label>
         <select name="warehouse_dist_id" class="form-control" required>
           @foreach($warehouses as $wh)
           <option value="{{$wh->id}}">{{$wh->name}}</option>
           @endforeach
         </select>
      </div>
      <div class="form-group mb-2">
         <label for="quantity">Quantity</label>
         <input required name="quantity" class="form-control" placeholder="{{$model->quantity}}" type="number"
         max="{{$model->quantity}}">
      </div>
      <input name="id" required type="hidden" value="{{$model->id}}">
    <a href="{{route('stocks.index')}}" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary" style="background: blue">Save</button>
    </form>
   </div>
@endsection