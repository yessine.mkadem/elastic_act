@extends('layouts.app')

@section('content')

    <div class="mt-4">
    <a href="{{route('stocks.create')}}" class="btn btn-success">New Stock</a>
    <a href="{{route('stocks.create')}}" class="btn btn-warning disabled">Consume Stock</a>
    </div>
    <div class="mt-4">
        <table class="table  table-striped table-bordered" >
            <thead>
                <tr>
                    <th>Wharehouse Name</th>
                    <th>Product name</th>
                    <th>Quantity</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($stocks as $coffee)
                <tr>
                    <td>{{ $coffee->warehouse->name }}</td>
                    <td>{{$coffee->product->name}}</td>
                    <td>{{$coffee->quantity}}</td>
                    <td>
                        <a href=""  class="btn btn-secondary">Achive</a>
                        <a href="{{route('stocks.edit',$coffee->id)}}" class="btn btn-info">Edit</a>
                        <a href="{{route('stocks.transfer',$coffee->id)}}" class="btn btn-success">Tansfer to Warehouse</a>
                        <a href="{{route('stocks.transferSalespoint',$coffee->id)}}" class="btn btn-success">Tansfer to Salespoint</a>      
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

 
@endsection