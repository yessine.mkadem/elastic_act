@extends('layouts.app')

@section('content')
<style>
   .view{
      width: 400px;
   }
</style>
<div class="view mt-4">
    <h1 class="mb-4">New Stock</h1>
    <form action="{{route('stocks.store')}}" method="post">
        @csrf
    <div class="form-group">
       <label for="warehouse_id" >Warehouse Name</label>
       <select name="warehouse_id" class="form-control" required>
         @foreach($warehouses as $wh)
         <option value="{{$wh->id}}" @if($wh->id == @$model->warehouse_id) selected @endif>{{$wh->name}}</option>
         @endforeach
       </select>
    </div>
    <div class="form-group">
      <label for="product_id" >Product Name</label>
      <select name="product_id" class="form-control" required>
        @foreach($products as $pr)
        <option value="{{$pr->id}}" @if($pr->id == @$model->product_id) selected @endif>{{$pr->name}}</option>
        @endforeach
      </select>
   </div>
    <div class="form-group mb-2">
       <label for="quantity">Quantity</label>
       <input required name="quantity" value="{{@$model->quantity}}"  class="form-control" type="number">
    </div>
    <input name="id" value="{{@$model->id}}"  type="hidden">
    <a href="{{route('stocks.index')}}" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary" style="background: blue">Save</button>
    </form>
   </div>
@endsection
