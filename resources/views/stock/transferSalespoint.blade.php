@extends('layouts.app')

@section('content')
<style>
   .view{
      width: 400px;
      
   }
</style>
<div class="view mt-4">
    <h1 class="mb-4">Transfer Stock</h1>
    <form action="{{route('stocks.saveTransferSales')}}" method="post">
        @csrf
    <div class="form-group">
       <label for="warehouse_id" >From Warehouse</label>
       <input name="warehouse_id" class="form-control" required type="text" readonly 
       value="{{$model->warehouse->name}}">   
    </div>
    <div class="form-group">
      <label for="product_id" >Transfer the Product </label>
      <input name="product_id" class="form-control" required type="text" readonly 
       value="{{$model->product->name}}">
    </div>
      <div class="form-group">
         <label for="salespoint_dist_id" >To the Salespoint</label>
         <select name="salespoint_dist_id" id="salespoint_dist_id" class="form-control" required>
            <option  readonly>Selectionnez une machine</option>
           @foreach($salespoint as $sa)
           <option value="{{$sa->id}}">{{$sa->name}}</option>
           @endforeach
         </select>
      </div>
       <div class="form-group">
         <label for="slot_dist_id" >To the Slot</label>
         <select name="slot_dist_id" id="slot_dist_id" class="form-control" required>
           {{-- @foreach($lots as $sl)
           <option value="{{$sl->id}}">{{$sl->name}}</option>
           @endforeach --}}

         </select>
      </div> 
      <div class="form-group mb-2">
         <label for="quantity">Quantity</label>
         <input required name="quantity" class="form-control" placeholder="{{$model->quantity}}" type="number"
         max="{{$model->quantity}}">
      </div>
      <input name="id" required type="hidden" value="{{$model->id}}">
    <a href="{{route('stocks.index')}}" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-primary" style="background: blue">Save</button>
    </form>
   </div>
   @endsection
   @section('js')
   <script>
      $('#salespoint_dist_id').on('change',function () {
            console.log($('#salespoint_dist_id').val())
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var formData = {
               salespoint_dist:$('#salespoint_dist_id').val()
            };

            $.ajax({
                type: "POST",
                url: '{{ route('stocks.getSlots') }}',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#slot_dist_id').html(data.html);
                      
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });
   </script>
@endsection

