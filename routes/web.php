<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WarehouseController;
use App\Http\Controllers\SalesPointController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\SalesPointStockController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {return view('dashboard');})->middleware(['auth'])->name('dashboard');


Route::group(['middleware' => 'auth'], function() {

    //warehousse route
    Route::get('warehouses/archiver/{id}',[WarehouseController::class,'archiver'])->name('warehouses.archiver');
   
   //salespoint route
   Route::get('salespoint/archiver/{id}',[SalesPointController::class,'archiver'])->name('salespoint.archiver');
   
   Route::get('salespoint/newslot',[SalesPointController::class,'newslot'])->name('salespoint.newslot');
   Route::post('stocks/newslotsave',[SalesPointController::class,'newslotsave'])->name('salespoint.newslotsave');
    
   
   //product route
   Route::get('product/archiver/{id}',[ProductController::class,'archiver'])->name('product.archiver');
   
   //clients route
   Route::get('client/archiver/{id}',[ClientController::class,'archiver'])->name('client.archiver');
    //stock transfer to warehouse route
   Route::get('stocks/transfer/{id}',[StockController::class,'transfer'])->name('stocks.transfer');
   Route::post('stocks/saveTransfer',[StockController::class,'saveTransfer'])->name('stocks.saveTransfer');
   //stock transfer to salespoint route
    Route::get('stocks/transferSales/{id}',[SalesPointStockController::class,'transferSales'])->name('stocks.transferSalespoint');
    Route::post('stocks/saveTransferSales',[SalesPointStockController::class,'saveTransferSales'])->name('stocks.saveTransferSales');
    
   Route::resource('warehouses', WarehouseController::class);

    // sales point route
    Route::resource('salespoint', SalesPointController::class);


    // product route
    Route::resource('product', ProductController::class);
    // client route
    Route::resource('client', ClientController::class);

    // stock route stocks.getSlots
    Route::post('stocks/getSlots',[StockController::class,'getSlots'])->name('stocks.getSlots');
    Route::resource('stocks', StockController::class);
});

require __DIR__.'/auth.php';
