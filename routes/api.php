<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\SalesPointsController;
use App\Http\Controllers\Api\SendDataController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/login',[AuthController::class,'login']);
Route::get('/logout',[AuthController::class,'logout']);
Route::get('/register',[AuthController::class,'register']);
Route::get('/salespoints',[SalesPointsController::class,'showSalespoints']);
// the routes to send data to api
Route::get('/salespointView',[SendDataController::class,'sendSalespoint']);
Route::get('salespointView/{id}',[SendDataController::class,'dataTransfer']);
///
Route::get('salespointView/{id}/{product_id}',[SendDataController::class,'selectProduct']);
// route to send data to nodemcu
Route::get('/nodemcudata',[SendDataController::class,'nodemcuData']);